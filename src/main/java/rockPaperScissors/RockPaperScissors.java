package rockPaperScissors;

import java.util.*;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> gameChoices = Arrays.asList("y", "n"); //multiple gameplays
    
    public void run() {
        while(true){
            System.out.println("Let's play round " + roundCounter);

            String humanChoice = playerChoice("Your choice (Rock/Paper/Scissors)? ").toLowerCase();
            String computerChoice = randomChoice(rpsChoices);

            System.out.printf("Human chose %s, computer chose %s. ", humanChoice, computerChoice);

            if(isWinner(humanChoice,computerChoice)) {
                System.out.println("Human wins!");
                humanScore++;
            }
            else if(isWinner(computerChoice, humanChoice)){
                System.out.println("Computer wins!");
                computerScore++;
            }
            else{
                System.out.println("It's a tie!");
            }
            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);

            String continueGame = severalRounds("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (continueGame.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
            roundCounter++;
        }

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    public boolean validInput(String userInput, List<String> choices){ //turn all input in to lowercase
        userInput = userInput.toLowerCase();
        return choices.contains(userInput);
    }

    public String randomChoice(List<String> choices){
        Random random = new Random();
        int randomIndex = random.nextInt(choices.size());
        String randomChoice = choices.get(randomIndex);
        return randomChoice;
    }
    public String playerChoice(String message){
        while(true){
            System.out.println(message);
            String choice = sc.next();
            if(validInput(choice, rpsChoices)){
                return choice;
            }
            else{
                return "I do not understand " + choice + ". Could you try again?";
            }
        }
    }
    public boolean isWinner(String choice1, String choice2){
        if(choice1 == "paper"){
            return choice2.equals("rock");
        }
        else if(choice1 == "scissors"){
            return choice2.equals("paper");
        }
        else if (choice1== "rock"){
        return choice2.equals("scissors");
        }
        else {
            return false;
        }
    }
    public String severalRounds(String message){
        while(true) {
            System.out.println(message);
            String yesNo = sc.next();
            if (validInput(yesNo, gameChoices)) {
                return yesNo;
            }
            else{
                System.out.println("I do not understand " + yesNo + ". Could you try again?");
            }
        }
    }
    }